@Web @Desktop @TheInternet @Authentication
Feature: [TheInternet][Web] Form Authentication
  As a user of the internet
  I should be able to authenticate using forms
  
  @Nominal @critical
  Scenario: Nominal case
    # ----- Step
    # ----- n° 1
    # ----- Open form authentication page
    # -----
    Given I want to use "Chrome" driver
    And I want to use url "https://the-internet.herokuapp.com/login"
    When I open web driver
    Then I should see element "xpath->//button//i[contains(text(), ' Login')]"