package com.apothuaud.selenium.bdd;

import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Context Object to be shared between step definitions
 *
 * It holds all scenario data
 */
public class ScenarioContext {

    // URL config
    private String urlTemplate;
    private String protocol;
    private String domain;
    private int port;
    private String endpoint;
    private String completeUrl;

    // API config
    private Method requestMethod;
    private RequestSpecification requestSpecification;
    private Response response;

    // Web client config
    private WebDriver webDriver;

    // Params
    private Properties secrets;
    private Map<String, String> scenarioParams;

    private static ScenarioContext instance = new ScenarioContext();

    public ScenarioContext(){

        setSecrets(new Properties());
        setScenarioParams(new HashMap<String, String>());
        setRequestSpecification(RestAssured
                .given()
                .config(RestAssuredConfig.newConfig().encoderConfig(EncoderConfig.encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false))));
    }

    public String getUrlFromScenario() {
        // Get the url template from ScenarioContext
        String url = ScenarioContext.getInstance().getUrlTemplate();

        // If the scenario is not designed to use url template
        if (url == null) {

            // simply get url complete url from scenario
            url = ScenarioContext.getInstance().getCompleteUrl();
        }

        // The scenario is designed to use url template
        else {

            if (ScenarioContext.getInstance().getProtocol() != null) {

                url = url.replace("${PROTOCOL}", ScenarioContext.getInstance().getProtocol());
            }

            if(ScenarioContext.getInstance().getDomain() != null){

                url = url.replace("${DOMAIN}", ScenarioContext.getInstance().getDomain());
            }

            if(ScenarioContext.getInstance().getPort() > 0){

                url = url.replace("${PORT}", String.valueOf(ScenarioContext.getInstance().getPort()));
            }

            if(ScenarioContext.getInstance().getEndpoint() != null){

                url = url.replace("${ENDPOINT}", ScenarioContext.getInstance().getEndpoint());
            }
        }

        return url;
    }

    static void newInstance() {
        instance = new ScenarioContext();
    }

    public static ScenarioContext getInstance(){
        return instance;
    }

    private String getUrlTemplate() {
        return urlTemplate;
    }

    public void setUrlTemplate(String urlTemplate) {
        this.urlTemplate = urlTemplate;
    }

    private String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    private String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    private int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    private String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    private String getCompleteUrl() {
        return completeUrl;
    }

    public void setCompleteUrl(String completeUrl) {
        this.completeUrl = completeUrl;
    }

    public Method getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(Method requestMethod) {
        this.requestMethod = requestMethod;
    }

    public RequestSpecification getRequestSpecification() {
        return requestSpecification;
    }

    public void setRequestSpecification(RequestSpecification requestSpecification) {
        this.requestSpecification = requestSpecification;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Properties getSecrets() {
        return secrets;
    }

    private void setSecrets(Properties secrets) {
        this.secrets = secrets;
    }

    public Map<String, String> getScenarioParams() {
        return scenarioParams;
    }

    private void setScenarioParams(Map<String, String> scenarioParams) {
        this.scenarioParams = scenarioParams;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    void setWebDriver(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
}
