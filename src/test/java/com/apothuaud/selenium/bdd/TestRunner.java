package com.apothuaud.selenium.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = { "src/test/resources/features" },
        glue = { "com.apothuaud.selenium.bdd" },
        tags = { "not @Ignore" },
        plugin = { "pretty" }
)
public class TestRunner {
}
