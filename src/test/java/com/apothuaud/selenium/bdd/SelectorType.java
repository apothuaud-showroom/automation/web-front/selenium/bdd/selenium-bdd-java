package com.apothuaud.selenium.bdd;

public enum SelectorType {
    id, xpath
}
